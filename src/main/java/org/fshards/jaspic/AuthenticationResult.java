/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic;

import javax.security.auth.callback.Callback;
import javax.security.auth.message.AuthStatus;

/**
 *
 * @author gabor
 */
public class AuthenticationResult {

    private AuthStatus status;

    private String userId;

    private Callback[] callbacks;

    public AuthenticationResult(AuthStatus status) {
        this.status = status;
    }

    public AuthenticationResult(AuthStatus status, String userId) {
        this.status = status;
        this.userId = userId;
    }

    public AuthenticationResult(AuthStatus status, Callback[] callbacks) {
        this.status = status;
        this.callbacks = callbacks;
    }

    public AuthStatus getStatus() {
        return status;
    }

    public String getUserId() {
        return userId;
    }

    public Callback[] getCallbacks() {
        return callbacks;
    }

    public boolean isHandled() {
        return AuthStatus.SUCCESS.equals(this.getStatus()) || AuthStatus.SEND_CONTINUE.equals(this.getStatus());
    }

}
