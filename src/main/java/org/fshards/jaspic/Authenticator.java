/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.message.AuthException;
import javax.security.auth.message.MessageInfo;

/**
 *
 * @author gabor
 */
public interface Authenticator {

    public AuthenticationResult authenticate(MessageInfo messageInfo, Subject clientSubject, Subject serverSubject) throws AuthException;

}
