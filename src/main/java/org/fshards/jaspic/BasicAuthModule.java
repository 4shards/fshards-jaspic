/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.message.AuthException;
import javax.security.auth.message.AuthStatus;
import javax.security.auth.message.MessageInfo;
import javax.security.auth.message.MessagePolicy;
import javax.security.auth.message.callback.CallerPrincipalCallback;
import javax.security.auth.message.callback.GroupPrincipalCallback;
import javax.security.auth.message.module.ServerAuthModule;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gabor
 */
public class BasicAuthModule implements ServerAuthModule {

    private ServiceLoader<Authenticator> authenticationHandlers;

    private CallbackHandler handler;
    private static final Class[] supportedMessageTypes = new Class[]{HttpServletRequest.class, HttpServletResponse.class};

    public BasicAuthModule() {
        authenticationHandlers = ServiceLoader.load(Authenticator.class);
    }

    @Override
    public void initialize(MessagePolicy requestPolicy, MessagePolicy responsePolicy, CallbackHandler ch, Map options) throws AuthException {
        this.handler = ch;
    }

    @Override
    public Class[] getSupportedMessageTypes() {
        return this.supportedMessageTypes;
    }

    @Override
    public AuthStatus validateRequest(final MessageInfo messageInfo, final Subject clientSubject, final Subject serverSubject) throws AuthException {
        System.out.print("org.fshards.jaspic.BasicAuthModule.validateRequest()");
        boolean mandatory = Boolean.valueOf((String) messageInfo.getMap().get("javax.security.auth.message.MessagePolicy.isMandatory"));

        try {
            Iterator<Authenticator> handlers = authenticationHandlers.iterator();
            AuthenticationResult result = new AuthenticationResult(AuthStatus.FAILURE);
            while (handlers.hasNext() && !result.isHandled()) {
                result = handlers.next().authenticate(messageInfo, clientSubject, serverSubject);
            }

            if (result.isHandled() && result.getCallbacks() != null) {
                processWithCallbacks(result.getCallbacks());
            } else if (result.isHandled() && result.getUserId() != null) {
                messageInfo.getMap().put("javax.servlet.http.registerSession", Boolean.TRUE.toString());
                processWithUserId(result, clientSubject, serverSubject);
            } else if (mandatory) {
                try {
                    HttpServletResponse response = (HttpServletResponse) messageInfo.getResponseMessage();
                    response.sendError(HttpServletResponse.SC_FORBIDDEN);
                    return AuthStatus.SEND_FAILURE;
                } catch (IOException ex) {
                    throw (AuthException) new AuthException().initCause(ex);
                }
            }
        } catch (AuthException e) {
            e.printStackTrace();
            throw e;
        }

        return AuthStatus.SUCCESS;
    }

    private void processWithCallbacks(Callback[] callbacks) throws AuthException {
        try {
            handler.handle(callbacks);
        } catch (IOException | UnsupportedCallbackException ex) {
            throw (AuthException) new AuthException().initCause(ex);
        }
    }

    private void processWithUserId(AuthenticationResult result, Subject clientSubject, Subject serverSubject) throws AuthException {
        processWithCallbacks(new Callback[]{new CallerPrincipalCallback(clientSubject, result.getUserId()), new GroupPrincipalCallback(serverSubject, new String[]{"manager"})});
    }

    @Override
    public AuthStatus secureResponse(MessageInfo mi, Subject sbjct) throws AuthException {
        return null;
    }

    @Override
    public void cleanSubject(MessageInfo mi, Subject sbjct) throws AuthException {
    }

}
