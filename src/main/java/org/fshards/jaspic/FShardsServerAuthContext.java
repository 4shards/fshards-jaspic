/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic;

import java.util.Collections;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.message.AuthException;
import javax.security.auth.message.AuthStatus;
import javax.security.auth.message.MessageInfo;
import javax.security.auth.message.config.ServerAuthContext;

/**
 *
 * @author gabor
 */
public class FShardsServerAuthContext implements ServerAuthContext {

    private final BasicAuthModule authModule;

    public FShardsServerAuthContext(CallbackHandler callbackHandler) throws AuthException {
        authModule = new BasicAuthModule();
        authModule.initialize(null, null, callbackHandler, Collections.emptyMap());
    }

    @Override
    public AuthStatus validateRequest(MessageInfo messageInfo, Subject clientSubject, Subject serverSubject) throws AuthException {
        return authModule.validateRequest(messageInfo, clientSubject, serverSubject);
    }

    @Override
    public AuthStatus secureResponse(MessageInfo messageInfo, Subject serviceSubject) throws AuthException {
        return authModule.secureResponse(messageInfo, serviceSubject);
    }

    @Override
    public void cleanSubject(MessageInfo messageInfo, Subject subject) throws AuthException {
        authModule.cleanSubject(messageInfo, subject);
    }

}
