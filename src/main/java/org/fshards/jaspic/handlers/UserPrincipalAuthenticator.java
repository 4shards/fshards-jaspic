/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.message.AuthStatus;
import javax.security.auth.message.MessageInfo;
import javax.security.auth.message.callback.CallerPrincipalCallback;
import javax.servlet.http.HttpServletRequest;
import org.fshards.jaspic.AuthenticationResult;
import org.fshards.jaspic.Authenticator;

/**
 *
 * @author gabor
 */
public class UserPrincipalAuthenticator implements Authenticator {

    @Override
    public AuthenticationResult authenticate(MessageInfo messageInfo, Subject clientSubject, Subject serverSubject) {
        HttpServletRequest request = (HttpServletRequest) messageInfo.getRequestMessage();
        if (request.getUserPrincipal() != null) {
            Callback[] callbacks = new Callback[]{new CallerPrincipalCallback(clientSubject, request.getUserPrincipal())};
            return new AuthenticationResult(AuthStatus.SUCCESS, callbacks);
        } else {
            return new AuthenticationResult(AuthStatus.FAILURE);
        }
    }

}
