/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;

/**
 *
 * @author gabor
 */
class FBInspectTokenResponse implements FBResponse {

    private String appId;
    private String application;
    private Long expiresAt;
    private boolean valid;
    private Long issuedAt;
    private List<String> scopes = new ArrayList<String>();
    private String userId;

    private FBError error;

    public String getAppId() {
        return appId;
    }

    public String getApplication() {
        return application;
    }

    public Long getExpiresAt() {
        return expiresAt;
    }

    public boolean isValid() {
        return valid;
    }

    public Long getIssuedAt() {
        return issuedAt;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public FBResponse fromJson(JsonObject json) {
        JsonObject data = json.getJsonObject("data");
        JsonObject error = data.getJsonObject("error");
        if (error != null) {
            this.error = new FBError().fromJson(error);
        } else {
            this.appId = data.getString("app_id");
            this.application = data.getString("application");
            this.expiresAt = (long) data.getInt("expires_at");
            this.valid = data.getBoolean("is_valid");
            this.issuedAt = (long) data.getInt("issued_at");
            JsonArray scopes = data.getJsonArray("scopes");
            for (JsonValue value : scopes) {
                this.scopes.add(value.toString());
            }
            this.userId = data.getString("user_id");
        }
        return this;
    }

}
