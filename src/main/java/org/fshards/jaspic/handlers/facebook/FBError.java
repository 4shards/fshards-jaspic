/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import javax.json.JsonObject;

/**
 *
 * @author gabor
 */
public class FBError implements FBResponse {

    private int code;
    private String message;
    private int subcode;

    public FBError() {
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public int getSubcode() {
        return subcode;
    }

    @Override
    public FBError fromJson(JsonObject json) {
        this.code = Integer.parseInt(json.getString("code"));
        this.message = json.getString("message");
        this.subcode = Integer.parseInt(json.getString("subcode"));

        return this;
    }

}
