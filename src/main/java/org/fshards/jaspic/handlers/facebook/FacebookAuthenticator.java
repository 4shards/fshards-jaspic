/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.security.auth.Subject;
import javax.security.auth.message.AuthException;
import javax.security.auth.message.AuthStatus;
import javax.security.auth.message.MessageInfo;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fshards.jaspic.AuthenticationResult;
import org.fshards.jaspic.Authenticator;

/**
 *
 * @author gabor
 */
public class FacebookAuthenticator implements Authenticator {

    private static final Logger logger = Logger.getLogger(FacebookAuthenticator.class.getCanonicalName());

    private final Map<String, String> fbEndpoints = new HashMap<String, String>() {
        {
            put("access_token", "https://graph.facebook.com/v2.9/oauth/access_token");
            put("login_dialog", "https://www.facebook.com/v2.9/dialog/oauth");
            put("debug_token", "https://graph.facebook.com/v2.9/debug_token");
        }
    };

    @Override
    public AuthenticationResult authenticate(MessageInfo messageInfo, Subject clientSubject, Subject serverSubject) throws AuthException {
        HttpServletRequest request = (HttpServletRequest) messageInfo.getRequestMessage();
        HttpServletResponse response = (HttpServletResponse) messageInfo.getResponseMessage();

        if (request.getRequestURI().matches(".*/j_security_check_facebook")) {
            return processFacebookLogin(request, response);
        } else {
            return new AuthenticationResult(AuthStatus.FAILURE);
        }
    }

    private AuthenticationResult processFacebookLogin(HttpServletRequest request, HttpServletResponse response) throws AuthException {
        logger.info("trying Facebook authenticate");

        AuthenticationResult result;
        if (request.getParameter("code") != null) {
            result = processFacebookTokens(request, response);
        } else {
            result = processFacebookDialog(request, response);
        }

        return result;
    }

    private AuthenticationResult processFacebookDialog(HttpServletRequest request, HttpServletResponse response) throws AuthException {
        String appId = request.getServletContext().getInitParameter(FacebookAuthenticator.class.getCanonicalName() + ".APP_ID");

        try {
            response.sendRedirect("https://www.facebook.com/v2.9/dialog/oauth?"
                    + "client_id=" + appId
                    + "&redirect_uri=" + calculateRedirectURI(request)
            );
            return new AuthenticationResult(AuthStatus.SEND_CONTINUE);
        } catch (IOException ex) {
            throw (AuthException) new AuthException().initCause(ex);
        }
    }

    private AuthenticationResult processFacebookTokens(HttpServletRequest request, HttpServletResponse response) throws AuthException {
        String appId = request.getServletContext().getInitParameter(FacebookAuthenticator.class.getCanonicalName() + ".APP_ID");
        String appSecret = request.getServletContext().getInitParameter(FacebookAuthenticator.class.getCanonicalName() + ".APP_SECRET");
        String code = request.getParameter("code");

        FBAccessTokenResponse tokenResponse = fetchAccessToken(appId, request, appSecret, code);
        FBAccessTokenResponse appTokenResponse = fetchAppAccessToken(appId, request, appSecret);
        FBInspectTokenResponse inspectResponse = inspectAccessToken(tokenResponse.getAccessToken(), appTokenResponse.getAccessToken());
        if (inspectResponse.isValid() && inspectResponse.getAppId().equals(appId)) {
            return new AuthenticationResult(AuthStatus.SUCCESS, inspectResponse.getUserId());
        } else {
            return new AuthenticationResult(AuthStatus.FAILURE);
        }
    }

    private FBAccessTokenResponse fetchAccessToken(String appId, HttpServletRequest request, String appSecret, String code) throws AuthException {
        try {
            Map<String, String> parameters = new HashMap<>();
            parameters.put("client_id", appId);
            parameters.put("client_secret", appSecret);
            parameters.put("redirect_uri", calculateRedirectURI(request));
            parameters.put("code", code);
            return makeRequest("access_token", parameters, new FBAccessTokenResponse());
        } catch (IOException ex) {
            throw (AuthException) new AuthException().initCause(ex);
        }
    }

    private FBAccessTokenResponse fetchAppAccessToken(String appId, HttpServletRequest request, String appSecret) throws AuthException {
        try {
            Map<String, String> parameters = new HashMap<>();
            parameters.put("client_id", appId);
            parameters.put("client_secret", appSecret);
            parameters.put("redirect_uri", calculateRedirectURI(request));
            parameters.put("grant_type", "client_credentials");
            return makeRequest("access_token", parameters, new FBAccessTokenResponse());
        } catch (IOException ex) {
            throw (AuthException) new AuthException().initCause(ex);
        }
    }

    private FBInspectTokenResponse inspectAccessToken(String accessToken, String appAccessToken) throws AuthException {
        try {
            Map<String, String> parameters = new HashMap<>();
            parameters.put("input_token", accessToken);
            parameters.put("access_token", appAccessToken);
            return makeRequest("debug_token", parameters, new FBInspectTokenResponse());
        } catch (IOException ex) {
            throw (AuthException) new AuthException().initCause(ex);
        }
    }

    private static String calculateRedirectURI(HttpServletRequest request) throws UnsupportedEncodingException {
        return URLEncoder.encode(request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/j_security_check_facebook", "utf-8");
    }

    private <T extends FBResponse> T makeRequest(String url, Map<String, String> parameters, T response) throws IOException {
        String queryString = "";
        if (!parameters.isEmpty()) {
            queryString += "?";
            for (String key : parameters.keySet()) {
                queryString += key + "=" + parameters.get(key) + "&";
            }
        }

        URLConnection connection = new URL(this.fbEndpoints.get(url) + queryString).openConnection();
        InputStream fbresponse = connection.getInputStream();

        JsonReader reader = Json.createReader(fbresponse);
        JsonObject tokenResponse = reader.readObject();
        return (T) response.fromJson(tokenResponse);
    }

}
