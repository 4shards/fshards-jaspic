/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import java.security.Principal;
import javax.security.auth.callback.Callback;

/**
 *
 * @author gabor
 */
public class FBPrincipal implements Principal, Callback {

    private String name;

    FBPrincipal(String userId) {
        this.name = userId;
    }

    @Override
    public String getName() {
        return name;
    }

}
