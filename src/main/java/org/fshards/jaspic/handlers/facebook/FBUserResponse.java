/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import javax.json.JsonObject;

/**
 *
 * @author gabor
 */
class FBUserResponse implements FBResponse {

    private String userId;
    private String email;
    private String name;

    public FBUserResponse() {
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    @Override
    public FBResponse fromJson(JsonObject json) {
        return this;
    }

}
