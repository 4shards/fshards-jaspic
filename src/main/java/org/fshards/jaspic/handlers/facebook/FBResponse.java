/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import javax.json.JsonObject;

/**
 *
 * @author gabor
 */
public interface FBResponse {

    public FBResponse fromJson(JsonObject json);

}
