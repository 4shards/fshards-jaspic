/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fshards.jaspic.handlers.facebook;

import javax.json.JsonObject;

/**
 *
 * @author gabor
 */
class FBAccessTokenResponse implements FBResponse {

    private String accessToken;
    private String tokenType;
    private Long expiresIn;

    public FBAccessTokenResponse() {
    }

    public FBAccessTokenResponse(String accessToken, String tokenType, Long expiresIn) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public FBAccessTokenResponse fromJson(JsonObject json) {
        this.accessToken = json.getString("access_token", "");
        this.tokenType = json.getString("token_type", "");
        this.expiresIn = (long) json.getInt("expires_in", -1);
        return this;
    }
}
